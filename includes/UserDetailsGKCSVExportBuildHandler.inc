<?php

class UserDetailsGKCSVExportBuildHandler extends GKCSVExportBuildHandler {
  protected static $name = 'User Details (GK CSV)';

  public function __construct($options = array()) {
    parent::__construct($options);
  }

  /**
   * Implements ExportBuildHandler::getData().
   */
  public function getData() {
    // Get the data.
    $query = db_select('gk_rewards_user_details', 'ud')->fields('ud');

    $query->addField('r', 'rid', 'reward_id');
    $query->addField('r', 'type', 'reward_type');

    $query->leftJoin('gk_rewards_fulfilments', 'f', 'ud.rfid = f.rfid');
    $query->leftJoin('reward', 'r', 'f.rid = r.rid');

    if (!empty($this->options['date_from'])) {
      $query->condition('ud.created', strtotime($this->options['date_from']), '>=');
    }

    if (!empty($this->options['date_to'])) {
      $query->condition('ud.created', strtotime($this->options['date_to']), '<=');
    }

    $data = $query->execute()->fetchAll();
    return self::csvBuildRows(self::csvHeaderInfo(), $data);
  }

  /**
   * Overrides GKCSVExportBuildHandler::csvHeaderInfo().
   */
  public static function csvHeaderInfo() {
    return array_merge(parent::csvHeaderInfo(), array(
      'mail' => array(
        'real field' => 'mail',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'title' => array(
        'real field' => 'title',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'firstnames' => array(
        'real field' => 'first_name',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'lastname' => array(
        'real field' => 'last_name',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'gender' => array(
        'real field' => 'title',
        'handler' => 'GenderExportFieldHandler',
      ),
      'postcode' => array(
        'real field' => 'postcode',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'dob' => array(
        'real field' => 'date_of_birth',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'optin-brand' => array(
        'real field' => 'comms_opt_in_brand',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'optin-other' => array(
        'real field' => 'comms_opt_in_other',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'created' => array(
        'real field' => 'created',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'changed' => array(
        'real field' => 'changed',
        'handler' => 'DefaultExportFieldHandler',
      ),
    ));
  }
}
